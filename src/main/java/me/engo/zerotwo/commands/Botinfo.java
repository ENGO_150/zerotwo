package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Botinfo extends ListenerAdapter {
	
	public static String alias = "bi";
	public String month;
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "botinfo") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length < 2) {
				String text = Translate.getTranslate(language, "basic_warnings", "low_parameters");
				Context.getChannel().sendMessage(text).queue();
			} else if (messageSent.length > 2) {
				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
				Context.getChannel().sendMessage(text).queue();
			} else {
				User u = Context.getMessage().getMentionedUsers().get(0);

				if (u == null){
					String text = Translate.getTranslate(language, "basic_warnings", "low_parameters");
					Context.getChannel().sendMessage(text).queue();
					return;
				}

				if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("january")) {
					month = "January";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("february")) {
					month = "February";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("march")) {
					month = "March";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("april")) {
					month = "April";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("may")) {
					month = "May";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("june")) {
					month = "June";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("july")) {
					month = "July";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("august")) {
					month = "August";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("september")) {
					month = "September";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("october")) {
					month = "October";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("november")) {
					month = "November";
				} else if (u.getTimeCreated().getMonth().toString().toLowerCase().equals("december")) {
					month = "December";
				} else {
					month = u.getTimeCreated().getMonth().toString().toLowerCase();
				}

				if (u.isBot()) {
					EmbedBuilder em = new EmbedBuilder();
					em.setTitle("User Info");
					em.addField("Name", u.getName(), true);
					em.addField("Tag", u.getAsTag().replace(u.getName() + "#", ""), true);
					em.addField("ID", u.getId(), false);
					em.addField("Created", u.getTimeCreated().getDayOfMonth() + "." + month + "." + u.getTimeCreated().getYear(), false);
					em.setThumbnail(u.getAvatarUrl());
					em.setColor(new Color(c.Color));
					em.setFooter(c.footer1, c.footer2);
					Context.getChannel().sendMessage(em.build()).queue();
				} else {
					String text = Translate.getTranslate(language, "advanced_warnings", "bot_false");
					Context.getChannel().sendMessage(text + c.prefix + "userinfo`").queue();
				}
			}
		}
	}
}

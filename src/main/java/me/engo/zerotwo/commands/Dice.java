package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class Dice extends ListenerAdapter {
	
	public static String alias = "There's no aliases.";
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "dice")) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length > 1) {
				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
				Context.getChannel().sendMessage(text).queue();
			} else {

				Random rnd = new Random();

				int rnd1 = rnd.nextInt(6);
				int rnd2 = rnd.nextInt(6);
				int rnd3 = rnd.nextInt(6);

				int rndb1 = rnd.nextInt(6);
				int rndb2 = rnd.nextInt(6);
				int rndb3 = rnd.nextInt(6);

				int rndv = rnd1 + rnd2 + rnd3;
				int rndbv = rndb1 + rndb2 + rndb3;

				if (rndv > rndbv) {
					String text1 = Translate.getTranslate(language, "logs", "dice_won");
					String text2 = Translate.getTranslate(language, "logs", "dice_template");
					Context.getChannel().sendMessage(text1 + rndv + text2 + rndbv + ".").queue();
				}
				if (rndv < rndbv) {
					String text1 = Translate.getTranslate(language, "logs", "dice_lost");
					String text2 = Translate.getTranslate(language, "logs", "dice_template");
					Context.getChannel().sendMessage(text1 + rndv + text2 + rndbv + ".").queue();
				}
				if (rndv == rndbv) {
					String text1 = Translate.getTranslate(language, "logs", "dice_match");
					String text2 = Translate.getTranslate(language, "logs", "dice_template");
					Context.getChannel().sendMessage(text1 + rndv + text2 + rndbv + ".").queue();
				}

			}
		}
		
	}

}

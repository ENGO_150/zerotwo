package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;

public class Flip extends ListenerAdapter {
	
	public static String alias = "coin";
	public String side;
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;
		
		if (messageSent[0].equalsIgnoreCase(c.prefix + "flip") || messageSent[0].equalsIgnoreCase(c.prefix + alias)) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length < 2) {
				String text = Translate.getTranslate(language, "basic_warnings", "low_parameters");
				Context.getChannel().sendMessage(text).queue();
			} else if (messageSent.length == 2) {
				if (!(messageSent[1].toLowerCase().equals("tail")) && !(messageSent[1].toLowerCase().equals("head"))) {
					System.out.println(messageSent[1]);
					String text = Translate.getTranslate(language, "advanced_warnings", "invalid_coin_side");
					Context.getChannel().sendMessage(text + c.prefix + "flip [head/tail])").queue();
				} else {
					Random rnd = new Random();
					int rndm = rnd.nextInt(10);

					if (rndm <= 5) {
						side = messageSent[1].toLowerCase();
						String text1 = Translate.getTranslate(language, "logs", "flip_template");
						String text2 = Translate.getTranslate(language, "logs", "flip_won");
						Context.getChannel().sendMessage(text1 + side + text2).queue();
					} else {
						if (messageSent[1].toLowerCase().equals("head")) {
							side = "tail";
						}
						if (messageSent[1].toLowerCase().equals("tail")) {
							side = "head";
						}
						String text1 = Translate.getTranslate(language, "logs", "flip_template");
						String text2 = Translate.getTranslate(language, "logs", "flip_lost");
						Context.getChannel().sendMessage(text1 + side + text2).queue();
					}
				}
			} else {
				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
				Context.getChannel().sendMessage(text).queue();
			}
		}
	}

}

package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.List;

public class Covid extends ListenerAdapter {

    public static String alias = "corona";

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Config c = new Config();
        String[] args = event.getMessage().getContentRaw().split(" ");
        if (event.getAuthor().isBot()) return;

        if (args[0].equalsIgnoreCase(c.prefix + "covid") || args[0].equalsIgnoreCase(c.prefix + alias)) {

            String language;
            File languages = new File("Database/Language/" + event.getAuthor().getId());
            if (languages.exists()) {
                File[] languages_ = languages.listFiles();
                assert languages_ != null;
                language = languages_[0].getName();
            } else {
                language = "english_en";
            }

            try {
                File ban = new File("Database/Bans/" + event.getAuthor().getId());
                if (ban.exists()) {
                    EmbedBuilder em = new EmbedBuilder();
                    final String ban_reason;

                    File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
                    List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
                    ban_reason = radky_duvodu.get(0);

                    em.setDescription("Hey " + event.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
                    em.setFooter(c.footer1, c.footer2);
                    em.setColor(new Color(c.Color));
                    event.getChannel().sendMessage(em.build()).queue();
                    return;
                }
            } catch (IOException e){
                e.printStackTrace();
            }

            if (args.length < 2) {
                try {
                    URL url = new URL("https://api.covid19api.com/summary");
                    JSONTokener tokener = new JSONTokener(url.openStream());
                    JSONObject obj = new JSONObject(tokener);
                    JSONObject data = obj.getJSONObject("Global");
                    String deaths = String.valueOf(data.getInt("TotalDeaths"));
                    String recovered = String.valueOf(data.getInt("TotalRecovered"));
                    String confirmed = String.valueOf(data.getInt("TotalConfirmed"));

                    event.getChannel().sendMessage("There are " + confirmed + " confirmed, " + recovered + " recovered and " + deaths + " dead people.").queue();
                } catch (Exception e){
                    event.getChannel().sendMessage("Something went wrong. Try it again.").queue();
                }
            } else {
                String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
                event.getChannel().sendMessage(text).queue();
            }
        }
    }
}

package me.engo.zerotwo.commands;

import me.engo.zerotwo.Config;
import me.engo.zerotwo.handlers.Translate;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class Giveaway extends ListenerAdapter {
	
	public static String alias = "There's no aliases.";

	public File f = new File("Database/Giveaway/true");
	public File fi = new File("Database/Giveaway/false");
	
	@Override
	public void onGuildMessageReceived(GuildMessageReceivedEvent Context) {
		Config c = new Config();
		String[] messageSent = Context.getMessage().getContentRaw().split(" ");
		if (Context.getAuthor().isBot()) return;

		if (messageSent[0].equalsIgnoreCase(c.prefix + "giveaway")) {

			String language;
			File languages = new File("Database/Language/" + Context.getAuthor().getId());
			if (languages.exists()) {
				File[] languages_ = languages.listFiles();
				assert languages_ != null;
				language = languages_[0].getName();
			} else {
				language = "english_en";
			}

			try {
				File ban = new File("Database/Bans/" + Context.getAuthor().getId());
				if (ban.exists()) {
					EmbedBuilder em = new EmbedBuilder();
					final String ban_reason;

					File radky_duvodu_soubor = new File(ban.getPath() + "/reason");
					List<String> radky_duvodu = Files.readAllLines(radky_duvodu_soubor.toPath());
					ban_reason = radky_duvodu.get(0);

					em.setDescription("Hey " + Context.getAuthor().getAsMention() + ", it seems you're banned.\nReason: " + ban_reason);
					em.setFooter(c.footer1, c.footer2);
					em.setColor(new Color(c.Color));
					Context.getChannel().sendMessage(em.build()).queue();
					return;
				}
			} catch (IOException e){
				e.printStackTrace();
			}

			if (messageSent.length < 2) {

				boolean active;
				active = f.exists();
				if (active) {
					String text = Translate.getTranslate(language, "logs", "giveaway_on");
					Context.getChannel().sendMessage(text).queue();
				} else {
					String text = Translate.getTranslate(language, "logs", "giveaway_off");
					Context.getChannel().sendMessage(text).queue();
				}
			} else if (messageSent.length == 2) {
				if (Context.getAuthor().getId().equals("574992310048260097")) {
					switch (messageSent[1].toLowerCase()) {
						case "set":
							try {
								fi.delete();
								f.createNewFile();
								Context.getChannel().sendMessage("Giveaway was started.").queue();
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;

						case "del":
							try {
								f.delete();
								fi.createNewFile();
								Context.getChannel().sendMessage("Giveaway was deleted.").queue();
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;

						default:
							Context.getChannel().sendMessage("Invalid args[1]. '" + messageSent[1] + "'").queue();
							break;
					}
				} else {
					String text = Translate.getTranslate(language, "basic_warnings", "developer_false");
					Context.getChannel().sendMessage(text).queue();
				}
			} else {
				String text = Translate.getTranslate(language, "basic_warnings", "high_parameters");
				Context.getChannel().sendMessage(text).queue();
			}
		}
	}
}
